package br.com.senac.mail;

import java.net.URL;
import org.apache.commons.mail.*;

public class GmailEmailWorking {

    private static final String myEmailId = "alterar";
    private static final String myPassword = "alterar";
    private static final String senderId = "alterar";

    public static void main(String[] args) {

        enviarEmailHTML();
    }

    public static void enviarEmailSimples() {
        try {
            MultiPartEmail email = new MultiPartEmail();
            email.setSmtpPort(587);
            email.setAuthenticator(new DefaultAuthenticator(myEmailId, myPassword));
            email.setDebug(true);
            email.setHostName("smtp.gmail.com");
            email.setFrom(myEmailId);
            email.setSubject("Olá !");
            email.setMsg("Este é um e-mail de teste ...: -) \\ n \\ nVerifique os anexos que enviei. \\ N \\ nObrigado, \\ nFahim");
            email.addTo(senderId);
            email.setTLS(true);

            /*
            EmailAttachment attachment = new EmailAttachment();
            attachment.setPath("/caminho/tables.xlsx");
            attachment.setDisposition(EmailAttachment.ATTACHMENT);
            attachment.setDescription("Excel");
            attachment.setName("tables.xlsx");
            email.attach(attachment);
             */
            email.send();
            System.out.println("E-mail enviado!");
        } catch (Exception e) {
            System.out.println("Exception :: " + e);
        }
    }

    public static void enviarEmailHTML() {

        try {
            // Create the email message
            HtmlEmail email = new HtmlEmail();
            email.setSmtpPort(587);
            email.setHostName("smtp.gmail.com");
            email.addTo(senderId);
            email.setFrom(myEmailId);
            email.setSubject("Olá !");
            email.setAuthenticator(new DefaultAuthenticator(myEmailId, myPassword));
            email.setDebug(true);
            email.setTLS(true);

           
            email.setHtmlMsg(
                    String.format("<html>\n"
                            + "    <head>\n"
                            + "        <title>Suporte</title>\n"
                            + "        <meta charset=\"UTF-8\">\n"
                            + "    </head>\n"
                            + "    <body  >\n"
                            + "\n"
                            + "        <h2>Olá Fulano de tal ,  </h2>\n"
                            + "        <hr />\n"
                            + "        <p>A equipe comando recebeu seu e-mail e estamos trabalhando para atender sua solicitação.</p>\n"
                            + "        <h3>Sua Mensagem:</h3>\n"
                            + "        <p>Por favor não estou conseguindo enviar para meu git erro 403.</p>\n"
                            + "        <br/>\n"
                            + "        <br/>\n"
                            + "        <br/>\n"
                            + "\n"                          
                            + "    <br/>\n" 
                            + "    <b>\n"
                            + "        Atenciosamente, <br />\n"
                            + "        <br/>\n"
                            + "        Equipe Comandos.\n"
                            + "    </b>\n"
                            + "     <br/>\n"
                            + "    <br/>\n"
                            + "    <br/>\n"
                            + "    <hr />\n"
                            + "    <center>\n"
                            + "        <b>© COPYRIGHT 2019, COMANDOS DO BRASIL S.A.</b>\n"
                            + "    </center>\n"
                            + "\n"
                            + "\n"
                            + "\n"
                            + "\n"
                            + "\n"
                            + "</body>\n"
                            + "</html>", "dsds"));

            // set the alternative message
            email.setTextMsg("Seu Ciente de E-mail nao suporta mensagens HTML.");

            // send the email
            email.send();
            System.out.println("E-mail enviado!");
        } catch (Exception e) {
            System.out.println("Exception :: " + e);
        }
    }

}
